#!/usr/bin/env python3

import sys
from binaryninja import *

"""
Binary Ninja script to RC4 decrypt REvil's strings and saves the decrypted strings
as comments at they addresses they are referenced at. Shout out to team OALabs! Sergei
its time to join the darkside!

SHA256: 5f56d5748940e4039053f85978074bde16d64bd5ba97f6f0026ba8172cb29e93
Sample: https://bazaar.abuse.ch/sample/5f56d5748940e4039053f85978074bde16d64bd5ba97f6f0026ba8172cb29e93/
Episode: https://www.youtube.com/watch?v=l2P5CMH9TE0
"""

def main():
	bv = binaryninja.open_view(sys.argv[1])
	bv.update_analysis()

	# Write back to the same .bndb
	bv.create_database(sys.argv[1]) 

	br = BinaryReader(bv)

	# One line RC4 algo. Look ma, no Lambda :-)!!!
	rc4 = Transform["RC4"] 

	mw_rc4_addr = 0x00404e03
	func_xrefs = bv.get_code_refs(mw_rc4_addr)

	for xref in func_xrefs:
		# Use Medium Level IL to extract the arguments to the rc4 decrypt function and decrypt
		# the strings. mw_rc4(ptr_to_encrypted_str, key_offset, key_len, str_len)
		il = xref.function.get_low_level_il_at(xref.address).mlil
		if il.operation == MediumLevelILOperation.MLIL_CALL:
			ptr_to_encrypted_str = il.operands[2][0].value.value
			key_offset = il.operands[2][1].value.value
			key_len = il.operands[2][2].value.value
			str_len = il.operands[2][3].value.value
			try:
				br.seek(ptr_to_encrypted_str + key_offset)
				key = br.read(key_len)
				enc_data = br.read(str_len)
				decrypted = rc4.encode(enc_data, {"key":key})
				# Decrypted strings are a mix of unicode and ascii
				if b"\x00" in decrypted:
					# Save decrypted string as comment at xref address
					bv.set_comment_at(xref.address, decrypted.decode("utf16"))
					print("{0}->{1}".format(hex(xref.address), decrypted.decode("utf16")))
				else:
					bv.set_comment_at(xref.address, decrypted.decode("utf8"))
					print("{0}->{1}".format(hex(xref.address), decrypted.decode("utf8")))
			except TypeError:
				continue

	bv.save_auto_snapshot()

if __name__ == "__main__":
	main()
