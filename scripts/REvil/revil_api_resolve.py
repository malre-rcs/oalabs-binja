import sys
import json
from binaryninja import *

""" 
Binary Ninja script that resolves REvil's API hashes to their respective names and labels
the data variables to the resolved names. Again all credit goes to Sergei of team 
OALabs as this is a port of their works.

To run the script you will need to download the exports.json file . Note some exports
are not in the json file however the most common ones to malware are. Included is 
a link to the original IDA import resolver script that is included in the episode. 
Lastly I have included my bndb file with showing the resolved export names. 

SHA256: 5f56d5748940e4039053f85978074bde16d64bd5ba97f6f0026ba8172cb29e93
Sample: https://bazaar.abuse.ch/sample/5f56d5748940e4039053f85978074bde16d64bd5ba97f6f0026ba8172cb29e93/
IDA Import Resolver: https://gist.github.com/OALabs/fc68ad4d63fd68c910f32d66fa5e981d
Episode: https://www.youtube.com/watch?v=R4xJou6JsIE
"""

export_db = {}

def setup(json_file):
    global export_db
    exports_json = json.loads(open(json_file,'r').read())
    exports_list = exports_json['exports']
    for export in exports_list:
        api_hash = get_api_hash(export)
        export_db[api_hash] = export

def get_api_hash(fn_name):
    result = 0x2b
    for c in fn_name:
        result = ord(c) + 0x10f * result
    return result & 0x1fffff

def transform_hash(*args):
    if len(args) == 3:
        result = ((hash_value ^ key1) << 0x10) ^ hash_value ^ key2
    else:
        result = (hash_value << 10) ^ hash_value ^ key
    return result & 0x1fffff

def lookup_hash(*args):
    if len(args) > 1:
        t_hash = transform_hash(hash_value, key1, key2)
    else:
        t_hash = transform_hash(hash_value)
    return export_db.get(t_hash, "")


bv = binaryninja.open_view(sys.argv[1])
bv.create_database("{0}-bn.bndb".format(sys.argv[1]))
bv.update_analysis()

# Loop through MLIL instructions and find the constant '<< 0x1fffff'(0x15) which is 
# used to check hash the result. Get the address of the function the constant 
# is found in. Lastly grab all of the xrefs to this function.
for il in bv.mlil_instructions:
	try:
		if il.src.left.src.name == "eax":
			if il.src.operation == MediumLevelILOperation.MLIL_LSR:
				if il.src.right.value.value == 0x15:
					func = bv.get_functions_containing(il.address)
					xrefs = bv.get_code_refs(func[0].lowest_address)
	except AttributeError:
		continue

# Some samples of REvil have additional key constants so we need to check for and snag them. 
iat_name_resolve = func[0]
hash_consts = []
for il in iat_name_resolve.mlil_instructions:
	try:
		if il.src.operation == MediumLevelILOperation.MLIL_XOR:
			if il.src.right.value.type == RegisterValueType.ConstantValue:
				hash_consts.append(il.src.right.value.value)
	except (AttributeError, IndexError):
		continue

if len(hash_consts) == 2:
	key1 = hash_consts[0]
	key2 = hash_consts[1]
else:
	key = hash_consts[0]

# Take the xrefs and find the function with the loop that iterates over the iat and grab
# the table address and size
iat = []
for xref in xrefs:
	for il in xref.function.mlil_instructions:
		try:
			iat.append(il.operands[0].operands[1].value.value)
		except (AttributeError, IndexError):
			continue

setup("exports.json")

start_of_iat = iat[0]
iat_size = iat[1]

# If you don't subtract 4 you read beyond the iat
end_of_iat = (start_of_iat + iat_size) - 4 

br = BinaryReader(bv)
br.seek(start_of_iat)
offset = br.offset

# Resolve the hashes to api names
while offset <= end_of_iat:
	if offset in bv.data_vars:
		hash_value = br.read32()
		if len(hash_consts) > 1:
			api_name = lookup_hash(hash_value, key1, key2)
			offset = br.offset
		else:
			api_name = lookup_hash(hash_value, key)
			offset = br.offset
	offset += 4

	if api_name == "":
		continue
	# Rename variables with the resolved API name
	sym = Symbol(SymbolType.DataSymbol, offset, "{0}".format(api_name))
	bv.define_user_symbol(sym)

	print("{0}->{1}".format(hex(hash_value), api_name))
# Save new db file
bv.save_auto_snapshot() 
