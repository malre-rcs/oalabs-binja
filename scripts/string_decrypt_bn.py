#!/usr/bin/env python3

import sys
from binaryninja import (BinaryViewType, BinaryReader, BinaryWriter)

"""
Simple script that de-obfuscates the malware sample's imports and patches the binary

SHA256: 4eb33ce768def8f7db79ef935aabf1c712f78974237e96889e1be3ced0d7e619
Download: https://bazaar.abuse.ch/sample/4eb33ce768def8f7db79ef935aabf1c712f78974237e96889e1be3ced0d7e619/
Episode: https://www.youtube.com/watch?v=JPvcLLYR0tE
"""

def main():
	bv = BinaryViewType.get_view_of_file(sys.argv[1])
	bv.update_analysis_and_wait()
	print(bv.segments)

	# Start and end of the string table to be decrypted
	start = 0x4053a4
	end = 0x4056c4
	key = "I0L0v3Y0u0V1rUs"

	br = BinaryReader(bv)
	bw = BinaryWriter(bv)

	br.seek(start)
	bw.seek(start)
	offset = br.offset

	while offset <= end:
		char = br.read8()
		for k in key:
			char = char ^ ord(k)
		char = ((~ord(chr(char))) & 255)
		bw.write8(char)
		offset = br.offset


if __name__ == "__main__":
	main()




		

